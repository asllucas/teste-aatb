import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FormularioComponent } from "./components/formulario/formulario.component";
import { HeaderComponent } from './components/header/header.component';
import { PagesComponent } from './containers/pages/pages.component';
import { FooterComponent } from './components/footer/footer.component';

import { NzInputModule } from 'ng-zorro-antd/input';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDividerModule } from 'ng-zorro-antd/divider';

import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations:[
    FormularioComponent,
    HeaderComponent,
    PagesComponent,
    FooterComponent
  ],
  imports:[
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzInputModule,
    NzFormModule,
    NzSelectModule,
    NzCardModule,
    NzButtonModule,
    NzDividerModule,
    ToastrModule.forRoot()
  ],
  exports: [
    PagesComponent
  ]
})
export class PagesModule{

}
