import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({ providedIn: 'root' })
export class PagesApi {

  private API_URL = environment.API_CEP;


  constructor(private http: HttpClient){}


  getCep(cep: any): Observable<any>{
    return this.http.get<any>(`${this.API_URL}/${cep}/json`)
  }

}
