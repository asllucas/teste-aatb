import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PagesApi } from './../../api/pages.api';
import { MensagemService } from './../../services/mensagem.service';


@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  singupForm: FormGroup;

  endereco: string;
  cidade: string;
  uf: string;

  constructor(
    private formBuilder: FormBuilder,
    private api: PagesApi,
    private mensagemService: MensagemService) { }

  ngOnInit(): void {
    this.iniciarForm();
  }

  getCep(event: any) {
    const cep = event.target.value
    if (cep) {
      this.api.getCep(cep).subscribe((response: any) => {

        if (response) {
          this.endereco = response.logradouro
          this.cidade = response.localidade
          this.uf = response.uf
        }
      });
    }
  }

  private iniciarForm() {
    this.singupForm = this.formBuilder.group({
      nome: ['', [Validators.required]],
      email: ['', [Validators.required]],
      cep: ['', [Validators.required]],
      endereco: [this.endereco],
      cidade: [this.cidade],
      uf: [this.uf],
      numero: ['', [Validators.required]],
      complemento: ['', [Validators.required]],
      mensagem: ['', [Validators.required]]
    });
  }

  onSubmit() {

    for (const i in this.singupForm.controls) {
      if (this.singupForm.controls.hasOwnProperty(i)) {
        this.singupForm.controls[i].markAsDirty();
        this.singupForm.controls[i].updateValueAndValidity();
      }
    }

    if(!this.singupForm.valid){
      return;
    }else{
      this.mensagemService.showSuccess("Formulário enviado com sucesso!", "");
      this.singupForm.reset();
    }

  }
}


